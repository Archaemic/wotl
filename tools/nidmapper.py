import xml.sax
import sys

class NIDMapper(xml.sax.handler.ContentHandler):
	def __init__(self):
		self._type = None
		self._nid = None
		self._inFunction = False
		self.map = {}

	def startElement(self, name, attrs):
		self._type = name
		if name == 'FUNCTION':
			self._inFunction = True

	def endElement(self, name):
		self._type = None
		if name == 'FUNCTION':
			self._inFunction = False

	def characters(self, chars):
		if not self._inFunction:
			return
		if self._type == 'NID':
			self._nid = chars.strip()
		elif self._type == 'NAME':
			self.map[self._nid.lower()] = chars.strip()

nidmapper = NIDMapper()
with open('352_psplibdoc_201008.xml', 'r') as f:
	xml.sax.parse(f, nidmapper)

for l in sys.stdin:
	l = l.strip()
	print('{}: {}'.format(l, nidmapper.map[l]))
