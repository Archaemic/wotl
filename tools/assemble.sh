#!/bin/sh
FILE=`basename $1 .S`
BASE=0x09FA0000
psp-as $1 -o $FILE.o --defsym _base=$BASE
#psp-objcopy -F elf32-littlemips $FILE.o $FILE.ro --change-addresses=$BASE
psp-objcopy -O binary $FILE.o $FILE.bin
