#!/usr/bin/env python3

import sys
import os

def applyPatch(outfile, inbytes, offset):
	outfile.seek(offset)
	outfile.write(inbytes)

def walkPatches(outfile, indir, offset):
	for root, dirs, files in os.walk(indir):
		for name in files:
			foff, ext = os.path.splitext(name)
			if ext != '.bin':
				continue
			foff = int(foff, 16)
			with open(os.path.join(root, name), 'rb') as infile:
				applyPatch(outfile, infile.read(), foff + offset)

def patchElf(outfname, indir):
	with open(outfname, 'r+b') as outfile:
		walkPatches(outfile, indir, -0x8803fac)

def patchIso(outfname, indir):
	with open(outfname, 'r+b') as outfile:
		walkPatches(outfile, indir, 0x76d4054)

if __name__ == '__main__':
	fname = sys.argv[1].lower()
	indir = 'bin/patch'
	if fname.endswith('.bin'):
		patchElf(fname, indir)
	elif fname.endswith('.iso'):
		patchIso(fname, indir)
	else:
		print('Unrecognized file!')
