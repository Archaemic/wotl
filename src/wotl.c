/* Some instructions that exist in MIP32r2 or Allegrex but are not portable */
#ifndef NATIVE
/* ext extracts bits start : start + num and shifts them (?) into dst. */
#define EXT(dst, src, start, num) dst = (((src) >> (start)) & ((1 << (num)) - 1)
/* ins replaces bits start : start + num with the shifted (?) cotnents of src. */
#define INS(dst, src, start, num) dst = (dst) & ~(((1 << (start)) - 1) & ~((1 << ((start) - (num))) - 1)) | ((src) << (start))
/* cache does something to the cache. I don't know what, exactly. */
#define CACHE(len, offset)
#else
/* No clue if the native versions work, I'm not too familiar with inline ASM syntax */
#define EXT(dst, src, start, num) asm("ext %0, %1, " #start ", " #num : "=r"(dst) : "r"(src))
#define INS(dst, src, start, num) asm("ins %0, %1, " #start ", " #num : "r"(dst) : "r"(src))
#define CACHE(len, offset) asm("cache " #len ", 0(%0)" : : "r"(offset))
#endif

/* ((x << 6U) - x) << 2 reduces to x * 0xFC */
#define FC_SHIFT(x) ((x) * 0xFC)

struct WotlGeContext {
	int unk_00;
	int* list;
	int* stall;
	int size;
	int index;
	int* unk_14[33];
	int fbPsm;
	int pad2;
	int vpWidth;
	int vpHeight;
	int pad3[20];
	int tail;
};

struct WotlMat4 {
	int xx, xy, xz, xw;
	int yx, yy, yz, yw;
	int zx, zy, zz, zw;
	int wx, wy, wz, ww;
};

/*
	bss: 0x8baf76c - ???
*/
struct WotlDrawContext {
	int unk_00;            // 0x8baf76c
	int unk_04;            // 0x8baf770
	int pad0;
	int activeIndex;       // 0x8baf778
	struct WotlGeContext* unk_10; // 0x8baf77c
	int pad1;
	int qid0;              // 0x8baf784
	int qid1;              // 0x8baf788
	int cbid;              // 0x8baf78c
	int fbPsm;             // 0x8baf790
	int fbWidth;           // 0x8baf794
	void* framebuffer;     // 0x8baf798
	int pad2[3];
	int vpWidth;           // 0x8baf7a8
	int vpHeight;          // 0x8baf7ac
	int unk_44;            // 0x8baf7b0
	struct WotlGeContext* activeGeContext;
	struct WotlGeContext geContext[5];
} globalDrawCtx;

/*
	text: 0x88047bc - 0x8804a20
	data:
		0x8a4c4e8: argument list for sceGeContextEnQueue
	unknown error constants: 0x80000001, 0x80000021
	calls: sceGeContextEnQueue, sceKernelCpuResumeIntr, sceKernelCpuSuspendIntr
	unknown calls: func_880513c, func_8805304, func_8805330, func_8805358, func_8805384
	called by: func_88352c0
*/
/* FIXME */
int func_88047bc(int index, void* geList, int size) {
	/* This function appears to have something to do with initializing the GE lists */
	struct WotlDrawContext* base = &globalDrawCtx;
	if (base->unk_00 == 0) {
		return 0x80000001;
	}
	int intr = sceKernelCpuSuspendIntr();
	int active = globalDrawCtx->geContext[index].tail;
	if (active >= 0) {
		sceKernelCpuResumeIntr(intr);
		base->ctxList[0].tail = 0;
		return 0x80000021;
	}

	base->geContext[index].unknown = base->activeIndex;
	base->activeGeContext = &base->geContext[index];
	base->activeIndex = index;
	sceKernelCpuResumeIntr(intr);
	if ((unsigned int) (index - 3) >= 2) {
		struct WotlDrawContext* ctx = base->activeGeContext;
		void* ptr = 0x40000000;
		void* oldBit;
		/* Set the uncached read flag */
		INS(ptr, geList, 0x0, 0x1d);
		/* Get the bits we didn't touch */
		EXT(oldBit, geList, 0x1e, 0x1);
		ctx->size = size;
		ctx->stall = ptr;
		ctx->list = ptr;
		if (oldBit == 0) {
			/* Clear the stall list */
			intr = sceKernelCpuSuspendIntr();
			CACHE(0x1b, geList);
			ctx = base->activeGeContext;
			while (1) {
				int* stall = ctx->stall;
				int* next = &stall[1];
				if (((long) stall) & 0x3f == 0) {
					sceKernelCpuResumeIntr(intr);
					break;
				}
				ctx->stall = next;
				stall[0] = 0;
			}
		}
	} else {
		struct WotlGeContext* ctx = base->activeGeContext;
		void* ptr;
		/* Get the cached version of the pointer */
		EXT(ptr, geList, 0x0, 0x1d);
		ctx->size = size;
		ctx->stall = ptr;
		ctx->list = ptr;
	}
	if (base->activeIndex == 0) {
		struct WotlGeContext* ctx = base->activeGeContext;
		base->unk_00 = 0;
		int qid = sceGeContextEnQueue(ctx->list, ctx->stall, base->cbid, 0x8a4c4e8);
		if (qid < 0) {
			return qid;
		}
		base->qid0 = qid;
	}
	if (base->unk_04 == 0)  {
		func_880513c(0x8a4c510);
		func_8805304(16, 16);
		func_8805330(7);
		$f20 = *0x8a71ce4;
		$f12 = $f20;
		func_8805358();
		$f12 = $f20;
		$f13 = $f20;
		func_8805384();
		base->unk_04 = 1;
	}
	if (base->activeIndex != 0) {
		return 0;
	}
	if (base->fbWidth == 0) {
		return 0;
	}

	/* Prepare graphics */
	struct WotlGeContext ctx = base->activeGeContext;
	base->geContext.fbPsm = base->fbPsm;
	ctx->vpWidth = base->vpWidth;
	ctx->vpHeight = base->vpHeight;
	int framebuffer = (int) base->framebuffer;
	int fbHi;
	EXT(fbHi, framebuffer, 0x18, 0x4);
	INS(framebuffer, 0, 0x18, 0x8);
	ctx->stall[0] = base->fbPsm | 0xd2000000;
	ctx->stall[1] = (fbHi << 16) | 0x9d000000 | base->fbWidth;
	ctx->stall[2] = framebuffer | 0x9c000000;
	ctx->stall = &ctx->stall[3];

	return 0;
}

/*
	Write the end instructions of a GE list
	arguments:
		signalId: the signal to raise when the list is done executing
	returns: < 0 on failure, the advanced number of bytes on success

	text: 0x8804a24 - 0x8804bc4
	data:
		0x8a4c550 - 0x8a4c560: jump table for switch
	unknown error constants: 0x80000021, 0x80000107
	calls: func_8807ebc, sceKernelCpuResumeIntr, sceKernelCpuSuspendIntr
	called by: func_8833840, func_8835340
*/
void* func_8804a24(int signalId) {
	int signalId = signalId & 0xffff;
	if (globalDrawContext->activeGeContext->index != 0) {
		return 0x80000021;
	}
	unsigned int index = globalDrawContext->activeIndex;
	if (index >= 5) {
		return 0x80000107;
	}
	switch (index) {
	case 0:
		struct WotlGeContext* ctx = globalDrawContext->activeGeContext;
		int* list = ctx->stall;
		list[0] = signalId | 0xf000000;
		list[1] = 0xc000000;
		ctx->stall = &ctx->stall[2];
		int result = func_8807ebc();
		if (result < 0) {
			return result;
		}
		break;

	case 1:
	case 3:
		if (globalDrawContext->unk_10 == 1) {
			struct WotlGeContext* ctx = globalDrawContext->activeGeContext;
			int* list = ctx->stall;
			list[0] = 0xe120000;
			list[1] = 0xc000000;
			ctx->stall = &ctx->stall[2];
		} else {
			struct WotlGeContext* ctx = globalDrawContext->unk_10;
			int* list = ctx->stall;
			list[0] = 0xb000000;
			ctx->stall = &ctx->stall[1];
		}
		break;

	case 2:
	case 4:
		struct WotlGeContext* ctx = globalDrawContext->activeGeContext;
		int* list = ctx->stall;
		list[0] = signalId | 0xf000000;
		list[1] = 0xc000000;
		ctx->stall = &ctx->stall[2];
		break;
	}
	struct WotlGeContext* ctx = globalDrawContext->activeGeContext;
	ptrdiff_t result = ctx->stall - ctx->list;
	int intr = sceKernelCpuSuspendIntr();
	int storedIndex = globalDrawContext->geContext[globalDrawContext->activeIndex].tail;
	globalDrawContext->geContext[globalDrawContext->activeIndex].tail = -1;
	globalDrawContext->activeIndex = storedIndex;
	if (storedIndex < 0) {
		globalDrawContext->activeGeContext = 0;
	} else {
		globalDrawContext->activeGeContext = &globalDrawContext->geContext[storedIndex];
	}
	sceKernelCpuResumeIntr(intr);
	return result;
}

/*
	Enqueue a GE list and context
	arguments:
		head: 1 if this is to be the head of a queue, 0 otherwise
		list: the address of the list to enqueue
		context: the GE context
		unknown0: unknown, appears to be passed to sceGeContextEnQueue
		unknown1: unknown, appears to be passed to sceGeContextEnQueue
	returns: 0 on success, a negative sce error code on failure

	text: 0x8804d30 - 0x8804dd0
	calls: sceGeContextEnQueueHead, sceGeContextQueueHead
	called by: func_8835340
*/
int func_8804d30(int head, const void* list, PspGeContext* context, int nStacks, void* stacks) {
	struct {
		int size;
		void* context;
		int nStacks;
		void* stacks;
	} args;
	args.size = 16;
	*((int*)0x8baf764) = 0;
	args.context = context;
	args.nStacks = nStacks;
	args.stacks = stacks;
	int result;
	if (head == 1) {
		result = sceGeContextEnQueueHead(list, 0, globalDrawContext->cbid, &args);
	} else {
		result = sceGeContextEnQueue(list, 0, globalDrawContext->cbid, &args);
	}
	if (result < 0) {
		return result;
	} else {
		globalDrawContext->qid1 = result;
		return 0;
	}
}

/*
	Set up the dither matrix
	arguments:
		dither: the dither matrix
	returns: none

	text: 0x880513c - 0x8805264
*/
void func_880513c(struct WotlMat4* dither) {
	struct WotlGeContext* ctx = globalDrawCtx->activeGeContext;
	ctx->stall[0] = ((dither->xw & 0xf) << 0xc) |
	                ((dither->xz & 0xf) << 0x8) |
	                ((dither->xy & 0xf) << 0x4) |
	                (dither->xx & 0xf) | 0xe2000000;

	ctx->stall[1] = ((dither->yw & 0xf) << 0xc) |
	                ((dither->yz & 0xf) << 0x8) |
	                ((dither->yy & 0xf) << 0x4) |
	                (dither->yx & 0xf) | 0xe3000000;

	ctx->stall[2] = ((dither->zw & 0xf) << 0xc) |
	                ((dither->zz & 0xf) << 0x8) |
	                ((dither->zy & 0xf) << 0x4) |
	                (dither->zx & 0xf) | 0xe4000000;

	ctx->stall[3] = ((dither->ww & 0xf) << 0xc) |
	                ((dither->wz & 0xf) << 0x8) |
	                ((dither->wy & 0xf) << 0x4) |
	                (dither->wx & 0xf) | 0xe5000000;

	ctx->stall = &ctx->stall[4];
}

/*
	text: 0x8805268 - 0x8805300
	unknown error constants: 0x80000025
	calls: sceGeListUpdateStallAddr
*/
int func_8805268(void) {
	struct WotlDrawContext* drawCtx = globalDrawCtx;
	struct WotlGeContext* geCtx = globalDrawCtx->activeGeContext;
	int index = geCtx->index - 1;
	if (index < 0) {
		return 0x80000025;
	}
	int* stall = geCtx->stall;
	int* list = geCtx->unk_14[index];
	int upperBits;
	int base = (int) stall;
	EXT(upperBits, stall, 0x18, 0x4);
	INS(base, 0, 0x18, 0x8);
	getCtx->index = index;
	bjump = upperBits << 10 | 0x10000000;
	base = base | 0x9000000;
	list[1] = bjump;
	list[0] = base;
	if (globablDrawCtx->activeIndex == 0) {
		int result = sceGeListUpdateStallAddr(globalDrawCtx->qid0, geCtx->stall);
		if (result < 0) {
			return result;
		}
	}
	return 0;
}

/*
	Insert a jump to an offset in the future

	text: 0x88054c8 - 0x8805514
*/
int* func_88054c8(int offset) {
	struct WotlGeContext* ctx = globalDrawContext->activeGeContext;
	/* Align the offset to a multiple of 4 */
	offset += 3;
	INS(offset, 0, 0, 2);
	int* stall = ctx->stall;
	int base = 0x10000000;
	int jump = 0x8000000;
	int next = (int) stall + offset + 8;
	int hi, lo;
	EXT(hi, next, 0x18, 0x4);
	lo = next;
	INS(lo, 0, 0x18, 0x8);
	ctx->stall = (int*) next;
	stall[0] = (hi << 16) | base;
	stall[1] = lo | jump;
	return &stall[2];
}

/*
	text: 0x88059f0 - 0x8805a48
	unknown calls: func_8806d98
	called by: func_882c140
*/
void func_88059f0(int arg0) {
	struct WotlGeContext ctx = globalDrawContext->activeGeContext;
	func_8806d98(ctx, arg0, 0, ctx)
	globalDrawContext->unk_44 = globalDrawContext->unk_44 & ~(1 << arg0);
}

/*
	Draw a primitive
	See pspsdk/src/gu/doc/commands.txt for more information

	arguments:
		primType: the type of primitive
		vType: the type of the vertices
		vCount: the number of vertices to draw
		indices: the vertex indices
		vertices: the vertex locations
	returns: none (?)

	text: 0x8805c10 - 0x8805cdc
	calls: func_8807ebc
	called by: func_8835740
*/
void func_8805c10(int primType, int vType, int vCount, void* indices, void* vertices) {
	int iHi, vHi;
	EXT(iHi, indices, 0x18, 0x4);
	EXT(vHi, vertices, 0x18, 0x4);

	iHi = (iHi << 16) | 0x10000000;
	vHi = (vHi << 16) | 0x10000000;

	struct WotlGeContext* ctx = globalDrawCtx->activeGeContext;

	int kick = ((primType & 0x7) << 16) | vCount | 0x4000000;
	if (vType != 0) {
		INS(vType, 0, 0x18, 0x8);
		ctx->stall[0] = vType | 0x12000000;
		ctx->stall = &ctx->stall[1];
	}

	int iLo = indices;
	INS(iLo, 0, 0x18, 0x8);
	if (indices != 0) {
		ctx->stall[0] = iHi;
		ctx->stall[1] = iLo | 0x2000000;
		ctx->stall = &ctx->stall[2];
	}

	int vLo = vertices;
	INS(vLo, 0, 0x18, 0x8);
	if (vertices != 0) {
		ctx->stall[0] = vHi;
		ctx->stall[1] = vLo | 0x1000000;
		ctx->stall = &ctx->stall[2]
	}
	ctx->stall[0] = kick;
	ctx->stall = ctx->stall[1];

	func_8807ebc();
}

/*
	Enqueue GE commands to set the viewport dimensions
	arguments:
		x: the x coordinate
		y: the y coordinate
		width: the width of the viewport
		height: the height of the viewport
	returns: the adjusted stall address

	text: 0x880612c - 0x88061d0
	called by: func_8833840
*/
void* func_880612c(int x, int y, unsigned int width, unsigned int height) {
	union {
		float f;
		unsigned int i;
	} a, b, c, d;
	/* This converts an int to a GE float */
	a.f = x;
	b.f = y;
	c.f = (width + (width >> 31U)) >> 1;
	d.f = (-height + (-height >> 31U)) >> 1;
	unsigned int* list = globalDrawCtx->activeGeContext->stall;
	list[0] = c.i >> 8U | 0x42000000;
	list[1] = a.i >> 8U | 0x43000000;
	list[2] = b.i >> 8U | 0x45000000;
	list[3] = d.i >> 8U | 0x46000000;
	globalDrawCtx->activeGeContext->stall = &list[4];
	return globalDrawCtx->activeGeContext->stall;
}

/*
	text: 0x8806ab4 - 0x8806aec
	unknown calls: func_8807c58
	called by: func_8833840
*/
void func_8806ab4(int arg0, int arg1, int width, int height) {
	func_8807c58(globalDrawCtx->activeGeContext, arg0, arg1, width, height);
}

/*
	text: 0x8807ebc - 0x8807f10
*/
void* func_8807ebc(void) {
	struct WotlDrawContext* base = &globalDrawCtx;
	if (base->activeIndex != 0) {
		return 0;
	}
	struct WotlGeContext* ctx = base->activeGeContext;
	if (ctx->index != 0) {
		return 0;
	}
	if (sceGeListUpdateStallAddr(base->qid0, ctx->stall) > 0) {
		return 0;
	}
	return ctx;
}

/*
	text: 0x882b800 - 0x882b990
*/
int func_882b800(int arg0) {
	/* The return value from this function is directly correlated with slowdown */
	void (*callback)();
	if (arg0 == 0) {
		callback = *($gp - 32752);
		if (callback != 0) {
			callback();
		}

		int current = sceDisplayGetAccumulatedHcount()
		int diff = current - *($gp - 32616);
		if (diff < 0) {
			diff = -diff;
		}

		*($gp - 32616) = current;
		return (int) (current << 8 / 272.f);
	} else if (arg0 != 1) {
		if (arg0 < 2) {
			return sceDisplayGetAccumulatedHcount();
		}
		/* Pointless recursion? */
		int diff = func_882b800(1);
		int i = diff >> 8;
		if (diff < 0) {
			i = (diff + 255) >> 8;
		}
		do {
			++i;
			callback = *($gp - 32752);
			if (callback != 0)  {
				callback();
			}
			/* Removing the loop condition makes things run too fast */
		} while (i < arg0);

		int current = sceDisplayGetAccumulatedHcount();
		int diff = current - *($gp - 32616);
		if (diff < 0) {
			diff = -diff;
		}
		*($gp - 32616) = current;
		return (int) (diff << 8 / 272.f);
	} else {
		if (sceDisplayGetAccumulatedHcount() < 0) return 0;
		int diff = sceDisplayGetAccumulatedHcount() - *($gp - 32616);
		if (diff < 0) {
			diff = -(sceDisplayGetAccumulatedHcount() - *($gp - 32616));
		} else {
			diff = sceDisplayGetAccumulatedHcount() - *($gp - 32616);
		}
		return (int) (diff << 8 / 272.f);
	}
}

/*
	text: 0x882c140 - 0x882c204
	unknown heap: 0x9174088, 0x91740a8 (short), 0x91740aa (short)
	calls: func_88059f0, func_88342c0, func_8835340, func_88353c0
	unknown calls: func_88058f0, func_88065cc, func_88065f8, func_8806660, func_882d600, func_8835480
*/
void func_882c140(int* arg0) {
	int local0 = (unsigned int) (*arg0 << 4) >> 4U;
	if (*((int*) 0x9174088) == 2) {
		return;
	}
	if (func_88352c0() != 0) {
		func_8806ab4(0, 0, *((short*) 0x91740a8), *((short*) 0x91740aa));
 	}
	func_88353c0();
	func_88059f0(3);
	func_88065cc(1, 1);
	func_8806660(1, 1);
	func_88058f0(0);
	func_88058f0(17);
	func_88065f8(0, 1);
	func_8805998(4);
	func_882d600(local0);
	func_8835480();
	func_8835340();
}

/*
	text: 0x8833840 - 0x8833a44
	unknown heap: 0x90f2c80, 0x917409c, 0x91740a0, 0x91740ac (unsigned char)
	calls: func_88047bc
	unknown calls: func_8804eb0, func_8805998, func_880612c, func_8835880, func_88359c0, func_8835e40, func_88e5e80,
		func_8835ec0, func_8837e80
*/

void func_8833840(void) {
	/* This function has to do with preparing the GE list for redrawing the screen */
	if (*0x91740a0 == 0) {
		return;
	}
	if (*0x917409c != 0) {
		return;
	}
	if (*(unsigned char*) 0x91740ac != 0) {
		return;
	}
	*(unsigned char*) 0x91740ac = 1;
	int local0 = func_8804eb0();
	func_88359c0($gp - 23996);
	func_88047bc(0, 0x90f2c80, 4096);
	func_8805998(2);
	/* This function has to do with screen size: this is the PSP's screen dimensions */
	func_8806ab4(0, 0, 480, 272);
	func_880612c(2048, 2048, 480, 272);
	if (*(unsigned char*) ($gp - 23952) != 0) {
		func_8837e80(*(unsigned char*) ($gp - 23951), *(unsigned char*) ($gp - 23950), *(unsigned char*) ($gp - 23949));
	}
	if (local0 != *(unsigned char*) ($gp - 23880)) {
		*($gp - 23884) = 1;
		*($gp - 23860) = 1;
	} else {
		*($gp - 23884) = 0
		*($gp - 23860) = 0
	}
	if (func_8835ec0(32) == 0) {
		if (*0x91740a0 != 0) {
			if (func_8835ec0(16) != 0) {
				func_8804f78(3, *($gp - 23880) + (*($gp - 23860) << 2), *($gp - 23868));
				func_8835880();
			}
			*0x91740a0 = 0;
		}
		if (*(unsigned char*) ($gp - 23952) != 0) {
			func_8804f78(3, 0x110000, 512);
			func_8805998(2);
			func_8806ab4(0, 0, 320, 240);
			func_880612c(2048, 2048, 320, 240);
			func_8837e80(*(unsigned char*) ($gp - 23951), *(unsigned char*) ($gp - 23950), *(unsigned char*) ($gp - 23949));
			func_8804f78(3, *($gp - 23880) + (*($gp - 23860) << 2), *($gp - 23868));
		}
		func_8835e40(16);
	}
	func_8806ab4(0, 0, 480, 272);
	*0x917409c = 1;
	func_8804a24(1919);
	*(char*) (0x91740ac) = 0;
	func_8835e80(32);
}

/*
	Set up the lower bits for PS1 draw mode/environment setting packet
	returns: the ORed lower bits

	text: 0x8834500 - 0x883455c
*/
int func_8834500(int tp, int abr, int tx, int ty) {
	/* This function reduces quite a bit, so I don't know why half the stuff here is here */
	/* The bits distribute as such: 001132222, where each number is the argument number */
	if (ty >= 512) {
		ty &= 0x1ff;
	}
	if (tx >= 1024) {
		tx &= 0x3ff;
	}
	int local0 = (abr & 3) << 5;
	int local1 = (tp & 3) << 7;
	int local2 = (ty & 0x100) >> 4;
	int local3 = (tx & 0x3ff) >> 6;
	int local4 = (ty & 0x200) << 2;
	return (local0 | local1 | local2 | local3 | local4) & 0xffff;

	/* This reduces to
	int local0 = (abr & 3) << 5;
	int local1 = (tp & 3) << 7;
	int local2 = (ty & 0x100) >> 4;
	int local3 = (tx & 0x3ff) >> 6;
	return local0 | local1 | local2 | local3;
	*/
}

/*
	text: 0x8834ec0 - 0x8834ee0
	called by: func_8922ac0
*/
void func_8834ec0(unsigned char* arg0) {
	unsigned char local0 = arg0[3];
	arg0[3] = (local0 & 0xFFFFFF0F) | 0x30;
	arg0[7] = 0x60;
}

/*
	text: 0x88350c0 - 0x883517c
	called by: func_8922ac0
*/
void func_88350c0(int* list, int arg1, int arg2, int arg3, void* arg4) {
	struct {
		unsigned char unk_0;
		unsigned char unk_1;
		unsigned char unk_2;
		unsigned char unk_3;
		short unk_4;
		short unk_6;
	} *struct_arg4 = arg4;
	((unsigned char*) list)[7] = 0xe1;
	((unsigned char*) list)[3] = (((unsigned char*) list)[3] & 0xFFFFFF0F) | 0x20;
	int local0;
	int local1;
	if (arg1 == 0) {
		local0 = 0;
	} else {
		local0 = 1024;
	}
	if (arg2 == 0) {
		local1 = 0;
	} else {
		local1 = 512;
	}
	list[1] = (arg3 & 0x9ff) | local0 | local1 | 0xe1000000;
	int local2;
	if (arg4 == 0) {
		local2 = 0;
	} else {
		int local3 = ~(struct_arg4.unk_4 - 1);
		local3 &= 0xff
		local3 >>= 3;

		int local4 = ~(struct_arg4.unk_6 - 1));
		local4 &= 0xff;
		local4 >>= 3;
		local4 <<= 5;

		/* This looks like a PS1 GPU instruction...uh oh */
		local2 = local3 | local4 | ((struct_arg4.unk_2 >> 3) << 15) | ((struct_arg4.unk_0 >> 3) << 10) | 0xe2000000;
	}
	list[2] = local2;
}

/*
	text: 0x8835180 - 0x88351a8
	called by: func_8922ac0
*/
void func_8835180(unsigned char* arg0, int arg1) {
	if (arg1 == 0) {
		arg0[7] &= ~2;
	} else {
		arg0[7] |= 2;
	}
}
/*
	text: 0x88352c0 - 0x883530c
	unknown heap: 0x9174088, 0x9174098
	called by: func_88047bc, func_882c140
*/
int func_88352c0(void) {
	if (*((int*) 0x9174088) != 0) {
		return 0;
	}
	*((int*) 0x9174088) = 1;
	*((int*) 0x9174098) = 1;
	func_88047bc(2, 0x90f3c80, 0x00080000);
	return 1;
}

/*
	Check to see if we want to draw this frame (if 0x9174088 == 1), and draw it if we do
	arguments: none
	returns: 1 if we drew, 0 otherwise

	text: 0x8835340 - 0x88353a4
	unknown heap: 0x9174088, 0x91740a0
	calls: func_8804a24, func_8804d30
	called by: func_882c140
*/
int func_8835340(void) {
	if (*((int*) 0x9174088) != 1) {
		return 0;
	}
	*((int*) 0x9174088) = 2;
	*((int*) 0x91740a0) = 1;
	func_8804a24(1192);
	func_8804d30(0, (void*) 0x90f3c80, 0, 32, 0x9173c80);
	return 1;
}

/*
	text: 0x88353c0 - 0x883546c
	unknown heap: 0x9174090, 0x91740a8 (short), 0x91740aa (short)
	calls: func_880612c, func_8806ab4
	unknown calls: func_88058f4, func_8805998, func_8835ec0
	called by: func_882c140
*/
int func_88353c0(void) {
	if (*0x9174090 != 0) {
		return 0;
	}
	if (func_8835ec0(32) == 0) {
		func_88058f4(3, 0x110000, 512);
	} else {
		func_88058f4(3, func_8835ec0(1), 512)
	}
	func_8805998(2);
	func_8806ab4(0, 0, *((short*) 0x91740a8), *((short*) 0x91740aa));
	func_880612c(2048, 2048, *((short*) 0x91740a8), *((short*) 0x91740aa));
	*0x9174090 = 1;
	return 1;
}

/*
	text: 0x8835740 - 0x8835874
	calls: func_8805c10, sceGeEdramGetAddr
	unknown calls: func_88054c8, func_880678c
*/
void func_8835740(int arg0) {
	short textureX = *(short*) ($gp - 23840);
	short textureY = *(short*) ($gp - 23838);
	short textureW = *(short*) ($gp - 23836);
	short textureH = *(short*) ($gp - 23834);
	short polyX = *(short*) ($gp - 23832);
	short polyY = *(short*) ($gp - 23830);
	short polyW = *(short*) ($gp - 23828);
	short polyH = *(short*) ($gp - 23826);

	int base = arg0 + (int) sceGeEdramGetAddr();
	short* vBase = func_88054c8(40);

	int textureX2 = (int) textureX + (int) textureW;
	int textureY2 = (int) textureY + (int) textureH;
	int polyX2 = (int) polyX + (int) polyW;
	int polyY2 = (int) polyY + (int) polyH;

	vBase[0] = textureX;
	vBase[1] = textureY;
	vBase[2] = polyX;
	vBase[3] = polyW;

	vBase[5] = textureX2;
	vBase[6] = textureY;
	vBase[7] = polyX2;
	vBase[8] = polyY;

	vBase[10] = textureX;
	vBase[11] = textureY2;
	vBase[12] = polyX;
	vBase[13] = polyY2;

	vBase[15] = textureX2;
	vBase[16] = textureY2;
	vBase[17] = polyX2;
	vBase[18] = polyY2;

	func_880678c(0, 512, 512, 512, base);
	func_8805c10(4, 0x800102, 4, 0, vBase);
}

/*
	text: 0x88361c0 - 0x8836214
*/
void func_88361c0(const short* arg0, const short* arg1) {
	if (arg0 != 0) {
		*($gp - 23840) = arg0[0];
		*($gp - 23838) = arg0[1];
		*($gp - 23836) = arg0[2];
		*($gp - 23834) = arg0[3];
	}
	if (arg1 != 0) {
		*($gp - 23832) = arg1[0];
		*($gp - 23830) = arg1[1];
		*($gp - 23828) = arg1[2];
		*($gp - 23826) = arg1[3];
	}
}

/*
	text: 0x8922540 - 0x8922658
	calls: func_89385c0
	unknown calls: func_8922680
*/
/* FIXME */
int func_8922540(int arg0) {
	$v1 = *0x8a97e84;
	$s2 = arg0;
	$s1 = *$v1;
	if (arg0 == 34) {
		func_89385c0(190, 0, 0, 0);
		func_89385c0(177, 0, 36, 0);
		$v1 = *0x8a97e84;
		$s3 = *$v1;
		*$v1 = $s1;
		$v0 = *0x8a97e84;
		*$v0 = $s0;
		func_89385c0(190, 0, 0, 0);
		func_89385c0(177, 0, 35, 0);
		$v0 = $s3 & 1;
		$v1 = $v0 << 15;
		$a1 = *0x8a97e84;
		$v0 = *$a1;
		$v0 = $v0 & 7;
		*$a1 = $s0;
		$v0 = $v0 << 12;
		$a1 = $v1 + $v0;
		func_8922680(34, $a1);
	}
	func_89385c0(190, 0, 0, 0);
	func_89385c0(177, 0, arg0, 0);
	$v1 = *0x8a97e84;
	$v0 = *$v1;
	*$v1 = $s1;
}

/*
	text: 0x8922ac0 - 0x8922bb0
	unknown bss: 0x8a97dcc
	unknown heap: 0x9384524, 0x938aaf4
	calls: func_8834500, func_8834ec0, func_88350c0, func_8835180, func_89e8d00
*/
void func_8922ac0(int arg0, char arg1) {
	if (arg1 == 0) {
		return;
	}
	func_8834ec0(arg0);
	func_8835180(arg0, 1);

	*(char*) (arg0 + 4) = 0;
	*(char*) (arg0 + 5) = 0;
	*(char*) (arg0 + 6) = 0;

	*(short*) (arg0 + 12) = 340;
	*(short*) (arg0 + 14) = 240;
	*(short*) (arg0 + 8) = 86;
	*(short*) (arg0 + 10) = 0;

	int local0 = 2;
	if (arg1 < 0) {
		local0 = 1;
		arg1 = -arg1;
	}
	int local1 = func_8834500(0, local0, 960, 256);
	func_88350c0((int*) (arg0 + 16), 0, 0, local1 & 0xffff, 0x8a97dcc);

	*(char*) (arg0 + 4) = arg1;
	*(char*) (arg0 + 5) = arg1;
	*(char*) (arg0 + 6) = arg1;

	if (*(unsigned short*) 0x9384524 == 0) {
		func_8938d00(arg0);
		func_8938d00(arg0 + 16);
	}
	else {
		func_8938d00(arg0 + 16);
		func_8938d00(arg0);
	}

	*(int*) 0x938aaf4 = 0;
}

/*
	text: 0x892e5c0 - 0x892f078
	calls: func_8922540, func_8922ac0, func_89385c0
	unknown calls: func_8833a80, func_8837a40, func_8881b00, func_8881ec0, func_8891100, func_88eed40, func_8911a80,
		func_8911c40, func_8912580, func_892e3c0, func_892f0c0, func_892f7c0, func_892f8c0, func_89669c0, func_8976400,
		func_8976980, func_89aa340, func_89bac40, func_89bae0c, func_89bb028, func_89f0100, func_8a20e80, func_8a214c0,
		func_8a2eb80, func_8a2ecc0
*/
/* FIXME */
func_892e5c0(arg0, arg1) {
	$s1 = arg0;
	$s0 = arg1;
	*0x974e6ec = *0x9729c18;
	if (*(unsigned short*) 0x9384524 != 0) {
		func_8833a80(0);
		$a0 = $sp + 24;
		*(short *) ($sp + 28) = 256;
		$v1 = *0x927520c & 1;
		$v0 = (($v1 << 4) - $v1) << 4;
		*(short *) ($sp + 26) = $v0
		*(short *) ($sp + 30) = 240;
		*(short *) ($sp + 24) = 0;
		func_88eed40($sp + 24, *0x8ae3458);
	}
	*0x938ab34 = arg0;
	$a0 = *0x9384544;
	$a0 = 0x93845fc + ((($a0 << 3) - $a0) << 2);
	func_8922ac0($a0, *0x9384568);
	func_892f8c0();
	func_8912580();
	$v0 = *(short *) 0x938aae8;
	if ($v0 != 0) {
		int i;
		for (i = 1; i < 16; ++i) {
			if (func_89bae0c(i) != 0) break;
		}
		if (i != 16) {
			*(short *) 0x938aae8 = 0;
		} else {
			$s1 = func_892e1c0();
			if ($s1 != 0 && func_8922540(508) != 0) {
				*(short*) 0x938aae8 = 0;
				$v0 = *0x8a97e84;
				$s0 = *$v0;
				func_89385c0(190, 508, 0, 0);
				func_89385c0(176, 508, 0, 0);
				$v1 = *0x8a97e84;
				*$v1 = $s0;
				return 9;
			}
			if ($s1 == 0) {
				*(short*) 0x938aae8 = 0;
				*(short*) 0x9384570 = 0;
				return 0;
			}
			if ($s1 == 0) {
				if (func_8922540(39) == 327) {
					return 19;
				}
				$v0 = *0x8a97e84;
				$s0 = *$v0;
				func_89385c0(190, 39, 0, 0);
				func_89385c0(176, 39, (($s1 & 0xc00) >> 2) + ($s1 & 0xff), 0);
				$a0 = ($s1 & 0xf300) >> 8;
				$a1 = *0x8a97e84
				*$a1 = $s0;
				*(short*) 0x9384570 = 0;
				*(short*) 0x938aae8 = 0
				switch ($a0) {
				case 128:
					return 9;
				case 129:
					return 10;
				case 130:
					return 11;
				case 131:
					return 12;
				}
				$v0 = *0x8a97e84;
				$s0 = *$v0;
				func_89385c0(190, 39, 0, 0);
				func_89385c0(176, 39, 0, 0);
				$v1 = *0x8a97e84;
				$s0 = *$v1;
				$v0 = *0x8a97e84;
				$s0 = *$v0;
				func_89385c0(190, 50, 0, 0);
				func_89385c0(176, 50, 0, 0);

				$a0 = *0x8a97e84;
				$s0 = *$a0;
				*0x9275248 = 4;
				return 9;
			}
			*(short *) 0x9384570 = 0;
			return 0;
		}
	}
	if (func_8a2eb80(4) == 0) {
		func_8911c40($s0);
	} else {
		func_8911c40(func_8a2ecc0() & 0xffff);
	}
	if (*(unsigned short*) 0x9384528 == 2) {
		*0x938aaf0 = *0x938aaf4;
		func_892e3c0(0x938aaf4);
		$v0 = *0x8a97e84;
		*0x938aaec = *0x938aaf4;
		*$v0 = $s0;
		func_89385c0(190, 42, 0, 0);
		func_89385c0(176, 42, 0, 0);
		$v0 = *0x8a97e84;
		*$v0 = $s0;
	}
	if (*(unsigned short*) 0x938451a >= 3) {
		*(unsigned short*) 0x938451a = 0;
	}
	if (*(unsigned short*) 0x938451c >= 3) {
		*(unsigned short*) 0x938451c = 0;
		*(short*) 0x9384580 = 1;
	}
	$v1 = -1
	*(short*) 0x938aba0 = -1;
	*(short*) 0x938aba2 = -1;
	*(short*) 0x938aba4 = -1;
	*(short*) 0x938aba6 = -1;
	*(short*) 0x938aba8 = -1;
	*(short*) 0x938aaea = 0;
	*(short*) 0x938451e = 0;
	*(short*) 0x9384520 = 0;
	*0x938ab14 = -1;
	*0x938ab18 = -1;
	*0x938ab1c = -1;
	*0x938ab20 = -1;
	*0x938ab24 = -1;
	*0x938ab28 = -1;
	*0x8a97e88 = -1;
	*0x8a97e90 = -1;
	*(short*) 0x8a97e8c = -1;
	*(short*) 0x8a97e94 = -1;
	*(short*) 0x8a97e96 = -1;
	if (func_8922540(508) != 0 && *(unsigned short*) 0x944c394 == 0 && func_8911a80() != 0) {
		if (func_89f0100(1) & 0x800 == 0) {
			*0x93844d4 = 0;
		} else {
			if (*0x93844d4 < 61) {
				++*0x93844d4;
			} else {
				*(short*) 0x944c394 = 1;
				func_8891100(113);
				*0x93844d4 = 0;
			}
		}
	}
	if (func_8922540(508) != 0 && *(unsigned short*) 0x938459a == 0 && func_8911a80() == 0 && *0x938aaf4 & 0x800 != 0) {
		if (func_8a20e80() == 6 && *0x934d5e8 == 0) {
				*(short*) 0x938459a = 1;
				func_8891100(113)
			}
		} else if (func_8a20e80() != 6 && func_8a214c0() != 5) {
			*(short*) 0x938459a = 1;
		}
	}
	if (*0x938453c == 3) {
		$v0 = func_8976980(*(short*) 0x8a7e2de, 0);
		*0x938aae4 = $v0;
		if ($v0 == 0) {
			*0x938343c = 0;
		} else {
			func_8837a40(*0x938ab34, $v0, $v0 + 252);
		}
	}
	if (*0x938453c == 4) {
		$v0 = func_8976400(*(short*) 0x8a7e2de, *0x938ab34 + 4);
		*0x938aae4 = $v0;
		if ($v0 < 2U) {
			*0x938453c = $v0;
		} else {
			func_8837a40(*0x938ab34, $v0, $v0 + 252)
		}
	}
	if (func_8911a80() == 0) {
		$v1 = *0x938aaf4
		if ($v1 != 256 && $v1 != 0) {
			*(short*) 0x9384578 = 5;
		}
	}

	$s0 = 1;
	$s1 = 0x938a9d9;
	for (; $s0 < 16; ++$s0, ++$s1) {
		*(char*) $s1 = func_89bae0c($s0)
		++$s0;
		++$s1;
	} while ($s0 < 16);
	func_89bac40();

	$s1 = 1;
	$s2 = 0x938a9d9;
	do {
		$s0 = *(unsigned char*) $s2;
		if ($s0 != func_89bae0c($s1)) {
			*(short*) 0x9384578 = 5;
		}
		++$s1;
		++$s2;
	} while ($s1 < 16);

	$v0 = *(unsigned short*) 0x9384578;
	if ($v0 != 0) {
		*(short*) 0x9384578 = $v0 - 1;
	}
	func_892f7c0();
	++*0x9384540;
	*0x9384544 = (*0x9384544 + 1) & 0x1;
	$v0 = *(unsigned short*) 0x9384548;
	if ($v0 != 0) {
		*(unsigned short*) 0x9384548 = $v0 - 1;
	}
	if (*(unsigned short*) 0x938ab98 != 0) {
		return 0;
	}
	$v1 = *0x938453c;
	if ($v1 != 1) {
		if ($v1 != 2) {
			if ($v1 == 5) {
				*0x8a97e78 = 1;
				func_8881b00(1);
				$s0 = *0x927520c;
				$v0 = *0x8A97e84;
				*$v0 = $s1;
				func_89385c0(190, 81, 0, 0);
				func_89385c0(176, 81, 0, 0);
				$v1 = *0x8a97e84;
				*$v1 = $s1;
				if (*0x93844d8 == 0) {
					func_89aa340(0);
				} else {
					func_89aa340(1);
				}
				*0x927520c = $s0;
				*0x938453c = 0;
			}
		} else {
			*0x8a97e78 = 1;
			func_8881b00(1);
			$s0 = func_89669c0(0, 0);
			func_8881ec0($s0);
			*(short*) 0x8a7e2de = $s0;
			*0x938453c = 0;
		}
	} else {
		*0x938453c = 0;
	}
	$v1 = *0x9384534;
	if ($v1 != 0 && *0x93845d8 == 0) {
		$a2 = $v1 << 2;
		*0x938abb4 = 0x887ea80;
		$v0 = 0x8a9a4d8 + $a2;
		$a0 = *$v0;
		$v0 = 0x8a9a518 + $a2
		$a1 = *$v0;
		$v1 = *0x8ae344c;
		$v0 = 0x8a9a558 + $a2;
		$v0 = *$v0;
		$a2 = $v1 + $v0;
		$v0 = func_89bb028($a0, $a1, $a2);
		if ($v0 == 0) {
			*0x93845b8 = 1;
		}
	} else if ($v1 != 0) {
		$v0 = *0x93845b8;
		if ($v0 != 0) {
			*0x938abb4 = 0x887eac0;
			/* FIXME: this takes arguments; where do they come from? */
			if (func_89bb028($a0, $a1, $a2) == 0) {
				*0x93845b8 = 0;
				*0x9384534 = 0;
			}
		}
	}
	$v1 = *0x9384538;
	if ($v1 != 0 && *0x93845d4 == 0) {
		*0x938abb4 = 0x887ea80;
		$v0 = 0x8a9a598 + $v1 << 2;
		$a0 = *$v0;
		$v0 = 0x8a9a5d8 + $v1;
		$a1 = *$v0;
		if (func_89bb028($a0, $a1, *0x938454c) == 0) {
			*0x93845b4 = 1;
		}
	} else if ($v1 == 0) {
		if (*0x93845d4 != 0) {
			*0x938abb4 = 0x887eac0;
			/* FIXME: this takes arguments; where do they come from? */
			if (func_89bb028(a0, $a1, $a2) == 0) {
				*0x93845b4 = 0;
				*0x9384538 = 0;
			}
		}
	}
	func_892f0c0();
	$v0 = func_8939040();
	$a0 = *(unsigned short*) 0x9384528;
	if ($a0 == $v0) {
		return $v0;
	}
	if ($a0 == 2) {
		return 0;
	}
	$s0 = $v0;
	do {
		$v0 = func_89bae0c($s0);
		if ($v0 != 0) break;
		++$s0;
	} while ($s0 < 16);

	if ($s0 == 16) {
		*(short*) 0x938aae8 = 1;
	}

	return 1;
}

/*
	text: 0x8938rc0 - 0x89388c0
*/
/* FIXME */
void func_89385c0(int opcode, int arg1, int arg2, int unused) {
	/* A third argument always seems to be passed to this function, but never used */
	if (opcode != 190)
		if (opcode & 1 != 0) {
			if (arg2 != 33) {
				$s0 = func_8938900(arg2);
				$v0 = func_89389c0(arg2);
				if ($v0 < 0) {
					$s3 = *$s0;
				} else {
					$v1 = (arg2 < 864) ? 1 : 15;
					$s3 = (*$s0 & ($v1 << $v0)) >> $v0;
				}
			} else {
				$s3 = func_893840();
			}
		} else {
			$s3 = arg2;
		}
	}

	$s6 = -1;
	$s1 = func_8938900(arg1);
	$s0 = func_89389c0(arg1);
	if ($s0 < 0) {
		$s2 = *$s1;
	} else {
		$a0 = ((arg2 < 864) ? 1 : 15) << $s0;
		$v1 = -1;
		$s6 = -1 - $a0;
		$s2 = (*$s1 & $a0) >> (unsigned int) $s0;
	}

	/* This seems to be the processing of arithmetic events */
	/* These values line up with 0xB0 - 0xBE in http://ffhacktics.com/instructions.php */
	if (opcode == 190) {
		$s2 = 0;
	} else if (opcode < 178) {
		$s2 = $s2 + $s3;
	} else if (opcode < 180} {
		$s2 = $s2 - $s3;
	} else if (opcode < 182) {
		$s2 = $s2 * $s3;
	} else if (opcode < 184) {
		if ($s3 == 0) {
			func_8955e80();
		}
		$s2 = $s2 / $s3;
	} else if (opcode < 186) {
		$s2 = $s2 % $s3;
	} else if (opcode < 188) {
		$s2 = $s2 & $s3;
	} else if (opcode < 190) {
		$s2 = $s2 | $s3;
	}

	if (*(unsigned char*) 0x93845bf == 0 && arg1 >= 960) {
			*(unsigned char*) 0x93845bf = 1;
			$v0 = func_8922540(arg1);
			*(unsigned char*) 0x93845bf = 0;
			if ($v0 >= 12U) return;
		}
	}

	if (*(unsigned char*) 0x93845bf == 0) {
		*(unsigned char*) 0x93845bf = 1;
		$v0 = func_8922540(508);
		*(unsigned char*) 0x93845bf = 0;
		if ($v0 != 0) {
			if (arg1 < 112) goto 0x89387e0
			if (arg1 < 144) goto 0x8938854

			0x89387e0:
			if (arg1 < 508) goto 0x89387f8
			if (arg1 < 512) goto 0x8938854

			0x89387f8:
			if (arg1 == 102) goto 0x8938854
			if (arg1 < 58) goto 0x893881c
			if (arg1 < 50) goto 0x8938854

			0x893881c:
			if (arg1 < 44) goto 0x8938854
			if (arg1 == 48) goto 0x8938854

			if (arg1 < 86) goto 0x893884c
			if (arg1 < 91) goto 0x8938854

			0x893884c:
			if (arg1 != 83) {
				return;
			}
		}
	}
	0x8938854:
	if ($s0 < 0) {
		*$s1 = $s2;
		if (arg1 != 25) return;
		*0x8a97df4 = $s2;
	}

	$v1 = $s2 & ((arg2 < 864) ? 1 : 15);
	$a0 = *$s1 & $s6;
	*$s1 = $a0;
	*$s1 = $a0 | ($v1 << $s0);
}

/*
	text: 0x8938d00 - 0x8938d74
	unknown heap: 0x9384524, 0x938ab34
*/
void func_8938d00(int* arg0) {
	if (*(unsigned short*) 0x9384524 != 0) {
		func_882c280(arg0);
		return;
	}

	*arg0 = (*arg0 & 0xf0000000) | (((*(unsigned int*) *(int*) 0x938ab34) << 4) >> 4U);
	*(unsigned int*) *(int*) 0x938ab34 = (((unsigned int) arg0 << 4) >> 4U) | (*(unsigned int*) *(int*) 0x938ab34 & 0xf0000000);
}

/*
	text: 0x8a08b80 - 0x8a08c54
*/
void func_8a08b80(short* arg0, short* arg1) {
	short local0[4];
	short local1[4];

	local1 = *(short*) 0x8b1dce0;
	local1 = *(short*) 0x8b1dce2;
	local1 = *(short*) 0x8b1dce4;
	local1 = *(short*) 0x8b1dce6;

	local0 = *(short*) 0x8b1dce8;
	local0 = *(short*) 0x8b1dcea;
	local0 = *(short*) 0x8b1dcec;
	local0 = *(short*) 0x8b1dcee;

	if (arg0 != 0) {
		local1[0] = arg0[0];
		local1[1] = arg0[1];
		local1[2] = arg0[2];
		local1[3] = arg0[3];
	}
	if (arg1 != 0) {
		local0[0] = arg1[0];
		local0[1] = arg1[1];
		local0[2] = arg1[2];
		local0[3] = arg1[3];
	}
	func_8835e40(8);
	func_88361c0(local1, local0)
}

/*
	text: 0x8a0ef00 - 0x8a0f07c
*/
/* FIXME */
int func_8a0ef00(int arg0) {
	int timing;
	if (*0x8a73c0c == 1) {
		func_8833a80(0)
		timing = func_882b800(0);
	} else {
		func_8833a80(0);

		if (*0x93e184c == 51 && *0x93e184c != 45) {
			timing = func_882b800(*0x8a73c0c);
		} else {
			int iterations;
			if (*0x9275218 >= 16) {
				iterations = 4;
				--*0x9275218;
			} else if (*0x9275218 == 0) {
				iterations = 2;
			} else {
				iterations = 3;
				--*0x9275218;
			}

			if (iterations >= *0x9275224) {
				timing = func_882b800(iterations);
			} else if (iterations != 1) {
				timing = func_882b800(iterations);
			} else {
				timing = func_882b800(0);
			}
		}
	}

	$a0 = *0x927520c;
	$v1 = $a0 << 2
	$v1 = $v1 + $a0
	$v0 = 0x9280c60
	$v1 = $v1 << 2
	func_8833700($v0 + $v1)
	$a0 = *0x927520c
	$v1 = $a0 << 1
	$v1 = $v1 + $a0
	$v1 = $v1 << 3
	$v1 = $v1 - $a0
	$v0 = 0x9280c88
	$v1 = $v1 << 2
	func_8833800($v0 + $v1)
	func_882c140(arg0)
	func_8833c00(-1)
	func_8a08e80()

	return timing;
}
